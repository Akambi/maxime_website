<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function response($etat, $message)
	{
		return $data = array("success" => $etat, "message"=> $message);
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function addcommande()
	{
		$don = array();
        parse_str($_POST['donner'], $params);

        $don["nom"] = $params["nom"];
        $don["prenom"] = $params["prenom"];
        $don["email"] = $params["email"];
        $don["telephone"] = $params["telephone"];

        if ($don["nom"] == "" || $don["prenom"] == "" || $don["email"] == "" || $don["telephone"] == ""  ) {

        	$response = $this->response(false, "Tout les champs sont réquis");

        }elseif (!filter_var($don["email"], FILTER_VALIDATE_EMAIL)) {

        	$response = $this->response(false, "Entrez une adresse mail valide");

        }elseif (!preg_match("/^(?:9[023456789]|6[012456789]|2[0123])[0-9]{6}$/", $don["telephone"])) {
        	
        	$response = $this->response(false, "Entrez un numero de téléphone valide");

        }else
        {
        	$data = ([
        		"nom" => $don["nom"],
        		"prenom" => $don["prenom"],
        		"email" => $don["email"],
        		"telephone" => $don["telephone"],
        		"dateCreation" => date('Y-m-d H:i:s'),
        		"dateModification" => date('Y-m-d H:i:s')
        	]);

        	if ($this->max->Ajout("commande", $data)) {
        		$response = $this->response(true, "Votre demande a été envoyer avec succès.");
        	}
        }

        echo json_encode($response);
	}

	public function SentMail()
	{

		$don = array();
        parse_str($_POST['donner'], $params);

        $don["name"] = $params["name"];
        $don["subject"] = $params["subject"];
        $don["message"] = $params["message"];

        if ($don["name"] == "" || $don["subject"] == ""  || $don["message"] == ""  ) {

        	$response = $this->response(false, "Tout les champs sont réquis");

        }else
        {	

			$subject =  $don["subject"];
			$message = $don["message"];
			$nom = $don["name"];

			$this->config->load('email');
			$this->email->from($this->config->item('smtp_user'), $nom);
			$this->email->to("ekpodilemaxime@gmail.com");
			$this->email->subject($subject);
			$this->email->message($message);


			if ($this->email->send()) {
				$response = $this->response(true, "Votre Mail a été envoyer avec succès.");
			}

        }

        echo json_encode($response);

	}

}
