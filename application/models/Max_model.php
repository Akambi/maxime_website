<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Max_model extends CI_Model {
	protected $date_array = array('dateCreation','dateModification');
	function __construct()
	{

		parent::__construct();//contructeur de la classe
		date_default_timezone_set("Africa/Porto-Novo");
	}

	//Fonction d'ajout d'enregistrements
	function Ajout($table,$data){

			$data_temp = $data;
			if ($this->date_array!=null || $this->date_array!="" ) {
				foreach ($this->date_array as $d ) {
					if (array_key_exists($d, $data_temp)) {
						unset($data_temp[$d]);
					}
				}
			}
    		

			if ($this->db->insert($table,$data)) {
				return array("success"=>true,"code"=>200,"data"=>$data,"message"=>"");
			}else{
				return array("success"=>false,"code"=>500,"data"=>null,"message"=>"");
			}
		
	}

}



	 
	 