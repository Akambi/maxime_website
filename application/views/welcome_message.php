<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>EKPODILE Maxime</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url();?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/lobibox.min.css"/>

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 ligne"></div>
    <div class="col-lg-5 col-md-5 col-sm-5 text-center" style=" border: none; font-size: 2em; ">
      MEILLEURE FORMATION EN GUITARE
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 ligne"></div>
  </div>
  </div>


   <header id="header" class="container">
    <div class="container-fluid">
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">ACCEUIL</a></li>
          <li><a href="#about">A PROPOS</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="container" style="margin-top: 1.2em;">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="carousel-background"><img src="<?php echo base_url();?>assets/img/logo.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>COUR EXCEPTIONNEL</h2>
                <h3 style="color: #fff">Formation Gratuite <br> <del id="souli">100$</del> <b>75$</b> </h3>

                <button class="btn video" data-video="<?php echo base_url();?>assets/video/presentation_maxime.mp4" data-toggle="modal" data-target="#videoModal">Bref Présentation <i class="fa fa-play-circle-o" aria-hidden="true"></i></button>

                <button class="btn video" data-toggle="modal" data-target="#info">Faites votre commande</button>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">

   
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Qui suis-je?</h3>
          <p>
Pétris de talents,  ça fait aujourd'hui environs 15 années que maxime EKPODILE joue à la guitare. il a déjà tourné sur de grandes scènes de jeu dans le monde musical. Aujourd'hui il a décidé de partager son savoir.

Dans cette série de 20 vidéos, il vous montre  tout le nécessaire pour bien jouer à la guitare. du vrai début jusqu'à un niveau très important.. à l'issue de cette formation, vous serez en mesure  de monter les cordes de votre guitare, jouer dans plusieurs gammes, vous apprendrez à jouer des accords et à créer plusieurs accords par vous même  et accompagner des morceaux, vous apendrez sur des thèmes, vous découvrirez le secret sur le pentatonique, le pentatonique blues, et pleins d'autres choses</p>
        </header>
      </div>
    </section>


    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Contactez-nous</h3>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Adresse</h3>
              <address>02 BP 257 Cotonou, BENIN</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Numero de teléphone</h3>
              <p><a href="tel:">+229-66-75-90-94</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:">ekpodilemaxime@gmail.com</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-group" class="text-center">
                <p id="message1"></p>
              </div>
            
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom Complet" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
             
            

          
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
              </div>
            

            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" placeholder="Votre Message"></textarea>
              <div class="validation"></div>
            </div>

             
           
          </form>

           <div class="text-center pull-left video"><button type="submit" class="sendmail">Envoyer</button></div>
        </div>


      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->


  <footer id="footer" class="container">
    

    <div class="container">
      
      <div class="copyright">
        &copy; <?php echo date('Y');?>, <strong id="name">EKPODILE Maxime</strong>
      </div>
    </div>
  </footer><!-- #footer -->
  
  <div class="container">
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  </div>



  <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <iframe width="100%" height="400" src="" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
</div>

  <div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Faites votre commande</h4>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="" method="post" id="form_add">

            <div class="form-group"  class="text-center">
              <p id="message" class="text-center"></p>
            </div>


            <div class="form-group">
              <label for="email" >*Email : </label>
              <input type="text" name="email" id="email" class="form-control">
            </div>

            <div class="form-group">
              <label for="nom" >*Nom : </label>
              <input type="text" name="nom" id="nom" class="form-control">
            </div>

            <div class="form-group">
              <label for="prenom" >*Prénom : </label>
              <input type="text" name="prenom" id="prenom" class="form-control">
            </div>

            <div class="form-group">
              <label for="telephone" >*Téléphone : </label>
              <input type="number" name="telephone" id="telephone" class="form-control">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="text-center pull-right "><button type="submit" class="btn btn-danger addcommande">Envoyer</button>
          </div>
        </div>
      </div>
    </div>
</div>
  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/lightbox/js/lightbox.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url();?>assets/contactform/contactform.js"></script>


  <script src="<?php echo base_url();?>assets/dist/js/lobibox.min.js"></script>
  <!-- If you do not need both (messageboxes and notifications) you can inclue only one of them -->
  <!-- <script src="dist/js/messageboxes.min.js"></script> -->
  <script src="<?php echo base_url();?>assets/dist/js/notifications.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url();?>assets/js/main.js"></script>



</body>
  <script>
  var base_url = "<?php echo base_url(); ?>";
  $(function() {
    $(".video").click(function () {
      var theModal = $(this).data("target"),
      videoSRC = $(this).attr("data-video"),
      videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal + ' button.close').click(function () {
        $(theModal + ' iframe').attr('src', "");
      });
    });
  });


  $('.addcommande').click(function() {

    $.post(
      base_url+'/index.php/Welcome/addcommande', 
      {donner: $('#form_add').serialize()}, 
      function(data, textStatus, xhr) {
        if (!data.success) 
        {
          $('#message').text(data.message);
        }else
        {
         
          Lobibox.notify("success", {
            msg: data.message,
            delay: 5000,
          });
          setTimeout(function(){

           location.reload(); 

         }, 5000);
          
        }


    }, "json").fail(function(jqXHR, error){
          alert(jqXHR.responseText);
    });
  });

  $('.sendmail').click(function() {
     $.post(
      base_url+'/index.php/Welcome/SentMail', 
      {donner: $('.contactForm').serialize()}, 
      function(data, textStatus, xhr) {
        if (!data.success) 
        {
          $('#message1').text(data.message);
        }else
        {
            Lobibox.notify("success", {
            msg: data.message,
            delay: 5000,
          });
          setTimeout(function(){

           location.reload(); 

         }, 5000);
        }
    }, "json").fail(function(jqXHR, error){
          alert(jqXHR.responseText);
    });
  });
  </script>
</html>
